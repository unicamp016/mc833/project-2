#include "lib.h"

double elapsed_time(struct timespec t1, struct timespec t2, int micro)
{
  double time_usec;

  time_usec = (t2.tv_sec - t1.tv_sec)*1e6 + (t2.tv_nsec - t1.tv_nsec)*1e-3;
  if (micro) {
    return time_usec;
  }
  else {
    return time_usec*1e-3;
  }
}

int send_str(int sockfd, char* buffer, int size, struct sockaddr* to)
{
  return sendto(
    sockfd,
    buffer,
    size,
    0,
    to,
    (socklen_t) sizeof(to)*2    /* Multiply by 2 in order to work with sockaddr_info */
  );
}


int recv_str(int sockfd, char* buffer, struct sockaddr* from)
{
  long int num_bytes;
  int fromlen;

  fromlen = sizeof(from);
  num_bytes = recvfrom(
    sockfd,
    buffer,
    BUFFER_LENGTH,
    0,
    from,
    (socklen_t *) &fromlen
  );

  /* Make sure we write a valid string to buffer */
  if (num_bytes < BUFFER_LENGTH) {
    buffer[num_bytes] = '\0';
  }

  return num_bytes;
}

int recv_profile(int sockfd, char* buffer, struct sockaddr* from)
{
  int error, retv;
  socklen_t fromlen;
  int image_size, recv_bytes, total_bytes;
  struct pollfd pol;
  char filename[200];
  FILE *image_fp;

  /* Fill out poll struct */
  pol.fd = sockfd;
  pol.events = POLLIN;

  /* Break in case query returned null */
  recvfrom(sockfd, &error, sizeof(int), 0, from, &fromlen);
  if (error > 0) {
    printf("Invalid email address. User not found (%d).\n", error);
    return error;
  }

  /* Wait for profile text info */
  recv_str(sockfd, buffer, from);
  printf("%s", buffer);


  /* Break in case there is no photo */
  recvfrom(sockfd, &error, sizeof(int), 0, from, &fromlen);
  if (error > 0) {
    printf("No photo available (%d).\n", error);
    return error;
  }

  /* Get image filename */
  recv_str(sockfd, buffer, from);
  sprintf(filename, "client/%s", buffer);

  /* Retrieve image size */
  recvfrom(sockfd, &image_size, sizeof(int), 0, from, &fromlen);

  /* Poll sockets waiting for there to be something to receive */
  total_bytes = 0;

  while (total_bytes < image_size) {
    retv = poll(&pol, 1, 3000);
    if (retv == -1) {
      perror("poll");
      break;
    }
    else if (retv == 0) {
      fprintf(stderr, "Timeout, datagram lost.\n");
      return -1;
    }
    else {
        recv_bytes = recvfrom(
            sockfd,
            buffer + total_bytes,
            MAX_DGRAM_SIZE,
            0,
            from,
            &fromlen
        );

        /* Only increment total bytes when recvfrom succeeds */
        if (recv_bytes >= 0) {
          total_bytes += recv_bytes;
        }
    }
  }

  /* Write the image to file only if we receive it entirely */
  if (recv_bytes == image_size) {
    image_fp = fopen(filename, "w");
    fwrite(buffer, image_size, 1, image_fp);
    fclose(image_fp);
  }
  return 0;
}
