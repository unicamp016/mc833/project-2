#include "lib.h"

int get_info(int sockfd, struct addrinfo *serv);

#define PORT "1337"

int main(int argc, char *argv[])
{
    int sockfd;
    struct addrinfo hints, *servinfo, *curr;
    int status;

    // Has to receive the name of the host as argument.
    if(argc != 2){
        fprintf(stderr, "Please state the name of the host\n");
        exit(1);
    }

    // Setting up hints for addrinfo
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;

    // Get server info
    if ((status = getaddrinfo(argv[1], PORT, &hints, &servinfo)) != 0){
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        return 1;
    }

    // Connect to the first possible
    for (curr = servinfo; curr != NULL; curr = curr->ai_next){

        // Create socket
        if ((sockfd = socket(curr->ai_family, curr->ai_socktype, curr->ai_protocol)) < 0){
            perror("client: socket");
            continue;
        }

        break;
    }

    freeaddrinfo(servinfo);

    // Verifying if socket exists.
    if (curr == NULL){
        fprintf(stderr, "client: failed to create socket\n");
        return 2;
    }

    printf("Ready to send...\n");

    // Gets profile info from server.
    get_info(sockfd, curr);

    // All done
    close(sockfd);

    return 0;
}

/**
 * Communicates with server to receive profile info.
 * Profile is linked to e-mail address.
 * @param sockfd Socket File Descriptor - Connection
 * @param serv   Server info.
 */
int get_info(int sockfd, struct addrinfo *serv){
    int num_bytes;
    int errno;
    char buffer[BUFFER_LENGTH];
    struct timespec t1, t2;
    double cli_time;
    FILE *log;

    log = fopen("logs/udp_cli_time.txt", "a");

    printf("E-Mail Address of profile: ");
    scanf("%s", buffer);

    clock_gettime(CLOCK_ID, &t1);

    /* Sends e-mail address corresponding to the profile. */
    num_bytes = send_str(sockfd, buffer, strlen(buffer) + 1, serv->ai_addr);

    if (num_bytes < 0){
        perror("client: send_str");
        exit(1);
    }

    /* Receive profile info */
    errno = recv_profile(sockfd, buffer, serv->ai_addr);

    clock_gettime(CLOCK_ID, &t2);
    cli_time = elapsed_time(t1, t2, CLIENT_UNIT);

    /* Write client time and errno as two separate columns */
    fprintf(log, "%g %d\n", cli_time, errno);
    fflush(log);
    fclose(log);

    return 0;
}
