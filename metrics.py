import os
import argparse
import subprocess

import pylab
import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt

# Logs data
log = True

# Directories
bin_dir = "bin"
log_dir = "logs"

# Filenames
stats_fn = "stats.txt"
dummy_fn = "dummy.txt"
client_fn = "cli_time.txt"
database_fn = "proc_time.txt"

emails = ['juja.princess@gmail.com', 'tutti.nervouser@yahoo.com']

def write_proc_times(n, host='localhost'):

    # Create dummy files for single udp and tcp request
    with open(os.path.join(bin_dir, f'tcp_{dummy_fn}'), "w") as f:
        f.write(f"6\n{emails[0]}\n0\n")

    with open(os.path.join(bin_dir, f'udp_{dummy_fn}'), "w") as f:
        f.write(f"{emails[0]}\n")

    # Compute n processing times for tcp
    tcp_dummy_fn = os.path.join(bin_dir, f'tcp_{dummy_fn}')
    for i in range(n):
        cmd = f"cat {tcp_dummy_fn} | make client_tcp ARGS={host}"
        subprocess.run(cmd, shell=True)

    # Compute n processing times for udp
    tcp_dummy_fn = os.path.join(bin_dir, f'udp_{dummy_fn}')
    for i in range(n):
        cmd = f"cat {tcp_dummy_fn} | make client_udp ARGS={host}"
        subprocess.run(cmd, shell=True)

def get_proc_times(server_type, plot_name=''):

    if len(plot_name) > 0:
        prefix = f'{plot_name}-{server_type}'
    else:
        prefix = f'{server_type}'

    cl_filename = os.path.join(log_dir, f'{prefix}_{client_fn}')
    db_filename = os.path.join(log_dir, f'{prefix}_{database_fn}')
    loss = 0;

    cl_file = open(cl_filename, 'r')
    db_file = open(db_filename, 'r')

    if server_type == 'tcp':
        cl_times = [float(line.strip()) for line in cl_file.readlines()]
        db_times = [float(line.strip()) for line in db_file.readlines()]
        total = len(db_times)
    else:
        cl_times, db_times = [], []
        db_lines = db_file.readlines()
        total = len(db_lines)
        for i, line in enumerate(cl_file.readlines()):
            tokens = line.strip().split(' ')
            if tokens[-1] == '0':
                cl_times.append(float(tokens[0]))
                db_times.append(float(db_lines[i].strip()))
            elif tokens[-1] == '-1':
                loss+=1

    cl_file.close()
    db_file.close()

    return np.array(db_times), np.array(cl_times), loss, total

def plot_data(server_type, plot_name=''):

    db_times, cl_times, loss, total = get_proc_times(server_type, plot_name)

    plot_gaussian(cl_times-db_times, server_type, "Tempo de Comunicação", plot_name)
    plot_gaussian(cl_times, server_type, "Tempo do Cliente", plot_name)
    plot_gaussian(db_times, server_type, "Tempo de Processamento", plot_name)

    if log:
        if len(plot_name) > 0:
            prefix = f'{plot_name}-'
        else:
            prefix = ''
        with open(os.path.join(log_dir, f'{prefix}{stats_fn}'), 'a') as st:
            st.write(f"Loss: {loss}/{total}\n\n")

def plot_gaussian(proc_times, server_type, name, plot_name):

    # Compute sample mean and sample standard deviation estimate
    mu = np.mean(proc_times)
    sigma = stats.tstd(proc_times)
    interv = (mu-sigma*1.96, mu+sigma*1.96)

    # Get plot filename from plot title.
    if name == 'Tempo de Comunicação':
        plt_name = 'communication'
    elif name == 'Tempo do Cliente':
        plt_name = 'client'
    elif name == 'Tempo de Processamento':
        plt_name = 'proc'

    # plt_name = '_'.join(map(str.lower, name.split(' ')))

    # Create just one figure and one subplot
    fig, ax = plt.subplots(figsize=(10,10))

    # Plot pdf relative likelihood for each point
    pdf = stats.norm.pdf(proc_times, mu, sigma)
    ax.scatter(proc_times, pdf, color=(1.0, 0.0, 0.0, 1.0))

    # Clip min and max y values
    ax.set_ylim(0, 1.1*max(pdf))

    # Create a new curve that represents a 95% confidence interval
    ci_xs = np.linspace(interv[0], interv[1], 30)
    ci_ys = stats.norm.pdf(ci_xs, mu, sigma)
    ax.fill_between(ci_xs, ci_ys, facecolor=(1.0, 0.0, 0.0, 0.4), label='95% IC')

    ax.legend()
    ax.set_ylabel('$f(x)$')
    ax.set_xlabel(f'{name} (ms)')

    if len(plot_name) > 0:
        prefix = f'{plot_name}-'
    else:
        prefix = ''
    fig.savefig(f'bin/{prefix}{server_type}-{plt_name}.pdf')

    # Prints to log file if required
    if log:
        with open(os.path.join(log_dir, f'{prefix}{stats_fn}'), 'a') as st:
            st.write(f"{name}\n")
            st.write(f"Server type: {server_type}\n")
            st.write(f"Mean: {mu}\n")
            st.write(f"Standard Deviation: {sigma}\n")
            st.write(f"Interval: {interv}\n")

def plot_stderr(type='client', plot_name=''):
    if len(plot_name) > 0:
        prefix = f'{plot_name}-'
    else:
        prefix = ''
    filename = os.path.join(log_dir, f'{prefix}{stats_fn}')

    # Store 3-uple with mean, lower, and upper interval
    xs = []
    ys = []
    yerrs = []
    xwords = []

    if type == 'client':
        start = 0
    elif type == 'communication':
        start = 5
    else:
        start = 10

    # Create list of stats and options
    with open(filename, 'r') as f:
        lines = f.readlines()
        for i in range(start, len(lines), 17):
            server_type = lines[i+1].split(':')[-1].upper()
            mean = float(lines[i+2].split(':')[-1])
            sigma = float(lines[i+3].split(':')[-1])
            xwords.append(server_type)
            xs.append(i//15 + 1)
            ys.append(mean)
            yerrs.append(sigma*1.96)

    ys = np.array(ys)
    yerrs = np.array(yerrs)

    # Plot graph with given stats
    fig, ax = plt.subplots(figsize=(10, 10))
    ax.errorbar(xs, ys, yerrs, linestyle='None', ecolor=(1.0, 0.0, 0.0, 0.5))
    ax.scatter(xs, ys)
    ax.set_ylim(0, 1.1*np.max(ys+yerrs))
    ax.set_xlim(np.min(xs)-1, np.max(xs)+1)
    ax.set_xticks(xs)
    ax.set_xticklabels(xwords)
    ax.set_ylabel('Tempo de Resposta (ms)')
    ax.set_xlabel('Servidor')

    if len(plot_name) > 0:
        prefix = f'{plot_name}-{type}'
    else:
        prefix = f'{type}'
    fig.savefig(f'bin/{prefix}-stderr.pdf')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--iterations', type=int, required=False, default=20,
                        help='Number of samples to run for each request type.')
    parser.add_argument('--host', required=False, default='localhost',
                        help='IP address of running servers')
    parser.add_argument('--plot', action='store_true',
                        help='Plot data given that all log files are available.')
    parser.add_argument('-n', '--plot_name', default='',
                        help='Log filenames prefix to use for plotting graphs.')

    args = parser.parse_args()

    if args.host == 'localhost' and not args.plot:

        subprocess.run("rm logs/*", shell=True)

        # Run experiments and write processing times to files
        write_proc_times(args.iterations, args.host)

        # Plot gaussian distribution for data
        plot_data('tcp', plot_name=args.plot_name)
        plot_data('udp', plot_name=args.plot_name)

        # Plot standard error comparison between server types
        plot_stderr('client', plot_name=args.plot_name)
        plot_stderr('communication', plot_name=args.plot_name)

    else:
        if not args.plot:
            subprocess.run("rm logs/*", shell=True)

            # Write processing times to files
            write_proc_times(args.iterations, args.host)

        else:
            # Plot gaussian distribution for data
            plot_data('tcp', plot_name=args.plot_name)
            plot_data('udp', plot_name=args.plot_name)

            # Plot standard error comparison between server types
            plot_stderr('client', plot_name=args.plot_name)
            plot_stderr('communication', plot_name=args.plot_name)
