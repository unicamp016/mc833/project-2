# Setup files and dirs
OUT_DIR = bin
SRC_DIR = src
REPORT_DIR = report
CLIENT_DIR = client
LOG_DIR = logs
UDP_DIR = udp
TCP_DIR = tcp
UDP_INCLUDE_DIR = $(UDP_DIR)/include
TCP_INCLUDE_DIR = $(TCP_DIR)/include
RESULT_DIR = results
ASSETS_DIR = assets

CLIENT = client
SERVER = server
REPORT = main

# Setup flags and libs
CFLAGS	= -Wall -Wextra -std=c99
SQLFLAGS = `mysql_config --cflags`
LIBS = -lm
SQLLIBS = `mysql_config --libs`

.PHONY: build client_tcp client_udp run_tcp run_udp clean reset_db reset_logs write-report rename

submit: build
	@mkdir $(OUT_DIR)/code
	@rsync -azP --exclude=".git*" --exclude-from=.gitignore . $(OUT_DIR)/code
	@tar -cz -C $(OUT_DIR) $(REPORT).pdf code -f $(OUT_DIR)/projeto-2.tar.gz
	@rm -rf $(OUT_DIR)/code

rename:
ifndef NAME
	$(error NAME is undefined)
endif
	@mkdir -p $(RESULT_DIR)
	@for file in $$(find $(OUT_DIR)/*.pdf $(LOG_DIR)/*.txt -type f ! -wholename "$(OUT_DIR)/$(REPORT).pdf"); \
		do mv $$file $(RESULT_DIR)/$(NAME)-$$(basename $$file); \
	done;

build: $(OUT_DIR)/udp_$(SERVER).o $(OUT_DIR)/udp_$(CLIENT).o $(OUT_DIR)/$(REPORT).pdf $(OUT_DIR)/tcp_$(SERVER).o $(OUT_DIR)/tcp_$(CLIENT).o

client_udp: $(OUT_DIR)/udp_$(CLIENT).o
	./$< $(ARGS)

run_udp: $(OUT_DIR)/udp_$(SERVER).o
	./$<

client_tcp: $(OUT_DIR)/tcp_$(CLIENT).o
	./$< $(ARGS)

run_tcp: $(OUT_DIR)/tcp_$(SERVER).o
	./$<

clean:
	rm -rf $(OUT_DIR)/* $(CLIENT_DIR)/* $(LOG_DIR)/*

write-report: $(REPORT_DIR)/$(REPORT).tex
	latexmk -pdf -pvc -cd $< -jobname=$(OUT_DIR)/$(REPORT)

reset_db:
	cat assets/profiles.sql | mysql -u root

reset_logs:
	rm -rf $(LOG_DIR)/*

$(OUT_DIR)/udp_$(SERVER).o: $(UDP_DIR)/$(SRC_DIR)/$(SERVER).c $(UDP_DIR)/$(SRC_DIR)/util.c $(UDP_DIR)/$(SRC_DIR)/socket.c
	@mkdir -p $(OUT_DIR) $(CLIENT_DIR) $(LOG_DIR)
	gcc $(CFLAGS) -I $(UDP_INCLUDE_DIR) $(SQLFLAGS) -o $@ $^ $(LIBS) $(SQLLIBS)

$(OUT_DIR)/udp_$(CLIENT).o: $(UDP_DIR)/$(SRC_DIR)/$(CLIENT).c $(UDP_DIR)/$(SRC_DIR)/socket.c
	@mkdir -p $(OUT_DIR) $(CLIENT_DIR) $(LOG_DIR)
	gcc $(CFLAGS) -I $(UDP_INCLUDE_DIR) -o $@ $^ $(LIBS)

$(OUT_DIR)/tcp_$(SERVER).o: $(TCP_DIR)/$(SRC_DIR)/$(SERVER).c $(TCP_DIR)/$(SRC_DIR)/util.c $(TCP_DIR)/$(SRC_DIR)/socket.c
	@mkdir -p $(OUT_DIR) $(CLIENT_DIR) $(LOG_DIR)
	gcc $(CFLAGS) -I $(TCP_INCLUDE_DIR) $(SQLFLAGS) -o $@ $^ $(LIBS) $(SQLLIBS)

$(OUT_DIR)/tcp_$(CLIENT).o: $(TCP_DIR)/$(SRC_DIR)/$(CLIENT).c $(TCP_DIR)/$(SRC_DIR)/socket.c
	@mkdir -p $(OUT_DIR) $(CLIENT_DIR) $(LOG_DIR)
	gcc $(CFLAGS) -I $(TCP_INCLUDE_DIR) -o $@ $^ $(LIBS)

$(OUT_DIR)/$(REPORT).pdf: $(REPORT_DIR)/$(REPORT).tex
	latexmk -bibtex -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)
