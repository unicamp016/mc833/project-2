\section{Método Experimental}\label{sec:method}
  Para medir o desempenho da comunicação cliente/servidor, foram coletadas \iterations~amostras do tempo total de consulta/atualização vistas pelo cliente ($T_t$) e do tempo de processamento para cada operação do ponto de vista do servidor ($T_p$). Como o tempo de processamento no servidor não está relacionado ao protocolo de comunicação, esse tempo é subtraido do tempo total do ponto de vista do cliente para obter uma estimativa do tempo de comunicação ($T_c = T_t - T_p$).

  As amostras foram coletadas sem concorrência, ou seja, havia um único cliente fazendo as requisições. O sistema foi testado de maneira automatizada, onde um arquivo com os comandos desejados é redirecionado para a entrada padrão do cliente. Nos testes em TCP, os testes foram feitos com conexões separadas. Isto é, para cada conexão TCP, é feita uma única requisição seguida pelo comando 0 para terminar a conexão. O tempo de estabelecer a conexão não é levado em consideração. As 100 requisições para o servidor UDP e TCP foram feitas para o mesmo email, que tinha uma foto de perfil correspondente.

  Foram realizados três tipos de testes: em LAN usando rede cabeada com protocolo de enlace Ethernet com o Cliente 2 da Tabela~\ref{tab:pc-stats}, em LAN usando rede sem fio com protocolo de enlace Wi-Fi com o Cliente 2, e em redes distintas (WAN) com o servidor e Cliente 1 em redes sem fio com protocolo Wi-Fi. A especificação das máquinas usadas para realizar os testes está indicada na Tabela~\ref{tab:pc-stats}. Como o banco de dados está armazenado no servidor, devemos levar em conta a especificação do servidor ao avaliar seu desempenho.

  \begin{table}[!ht]
      \begin{center}
        \caption{Especificação das máquinas dos clientes e servidor usadas para realizar os experimentos.}\label{tab:pc-stats}
        \resizebox{\textwidth}{!}{
        \begin{tabular}{|c|c|c|c|}
            \hline
            \textbf{Tipo} & \textbf{Cliente 1} & \textbf{Cliente 2} & \textbf{Servidor}\\
            \hline
            CPU & Intel Pentium Dual-Core & Intel Core i5-7300HQ & Intel Core i7-3770K \\
            Clock & 2.20 GHz & 2.50GHz & 3.50GHz\\
            Cores & 2 & 4 & 4\\
            RAM & 3GB 800MHz & 8GB 2400MHz & 32GB 1600 MHz \\
            Disco secundário & HDD 7200 rpm & SSD & SSD \\
            Interface de disco  & SATA 6GB & SATA 6GB & SATA 6GB \\
            Protocolo Wi-Fi & 802.11b/g & 802.11ac & 802.11n \\
            Ethernet (capacidade)       & N/A & 1Gbit/s & 1Gbit/s\\
            Taxa de transmissão WAN disponível & 50 Mbit/s & N/A & 10 Mbit/s\\
            \hline
            \end{tabular}
          }
      \end{center}
  \end{table}

\section{Resultados}
  Os resultados dos experimentos detalhados na Seção~\ref{sec:method} estão dispostos na Tabela~\ref{tab:metrics}. Para essa tabela, não foram considerados no cálculo da média e do desvio padrão as requisições UDP onde houve perdas. A partir das medidas dos tempos $T_c$, $T_p$ e $T_t$, foram gerados os gráficos das Figuras~\ref{fig:eth-stderr} a~\ref{fig:wan}. A seguir será apresentada uma discussão dos principais resultados dos experimentos.

    \subsection{Tamanho de Código e Nível de Abstração}
    O tamanho de código para os servidores TCP e UDP não é diretamente comparável, já que o servidor TCP implementa muito mais funcionalidade para tratar cinco outros tipos de requisição. Portanto, nos restringimos a avaliar a diferença nos arquivos \texttt{socket.c} de cada servidor, que implementa os formatos de mensagem para a transmissão de texto e dos dados do perfil discutidos na Seção~\ref{sec:server}. O arquivo \texttt{socket.c} do servidor TCP tem 75 linhas, enquanto o do UDP tem 128. Essa diferença é devida principalmente aos diferentes níveis de abstração. No TCP, há uma conexão, a comunicação é feita diretamente pelo socket da conexão. Já no UDP, é preciso armazenar as informações do destinatário diretamente e usá-las para enviar uma mensagem.
    
    Outra seção em comum é a função \texttt{send\_profile}, vista no arquivo \texttt{util.c}. Nessa função, a diferença em tamanho de código não é grande, com a versão para TCP tendo 153 linhas enquanto a do UDP tem 158 linhas. A principal diferença é o tratamento do envio da imagem.

    \subsection{Discussão}
    O tempo de resposta para uma requisição é uma variável aleatório contínua. O nosso objetivo é estimar a média da variável e encontrar o intervalo de confiança para essa estimativa da média. Usando o Teorema do Limite Central e assumindo uma distribuição normal, podemos estimar o valor da média e o erro padrão usando um conjunto de amostras através das Equações~\ref{eq:sample-mean} e~\ref{eq:sample-stdev}.

    \begin{equation}\label{eq:sample-mean}
        \mu_x = \frac{1}{n} \sum_{i=1}^n x_i
    \end{equation}

    \begin{equation}\label{eq:sample-stdev}
        \sigma_x = \frac{\sigma}{\sqrt{n}}
    \end{equation}

    Entretanto, como pode ser visto na Equação~\ref{eq:sample-stdev}, é necessário conhecer o desvio padrão da população ($\sigma$) para encontrar o erro padrão ($\sigma_x$). É claro que no caso dos tempos de resposta das requisições, esse parâmetro da população é desconhecido. Dessa forma, podemos optar por usar uma distribuição t de Student para computar o intervalo de confiança, que não requer o conhecimento prévio do desvio padrão da população, ou podemos usar uma estimativa para o desvio padrão da média. Por simplicidade, optamos pela segunda opção, onde usamos um estimador não enviesado para variância, do qual obtemos o erro padrão corrigido que é dada pela Equação~\ref{eq:unbiased-sample-stdev}~\cite{walpole}.

    \begin{equation}\label{eq:unbiased-sample-stdev}
        s_x = \sqrt{\frac{1}{n-1} \sum_{i=1}^n (x_i - \mu_x)^2}
    \end{equation}

    Usando a nossa estimativa para o erro padrão ($s_x$), podemos encontrar um intervalo de confiança de 95\% da média, isto é, um intervalo que tem 95\% de chance de conter a média real da população. Através da tabela para a função acumulada da distribuição normal, identificamos que o $Z$-score correspondente a esse intervalo de confiança é $1,96$. Portanto, podemos dizer que o intervalo $[\mu-1,96s_x, \mu+1,96s_x]$ tem 95\% de chance de conter a média da população. Os tempos $T_t$, $T_p$ e $T_c$ de cada teste estão dispostos na Tabela~\ref{tab:metrics} com a média, erro padrão e intervalo de confiança para cada variável. Na mesma tabela também está a quantidade de perdas para cada teste.

    Pode-se observar que o tempo de comunicação para os testes Wi-Fi LAN e WAN é aproximadamente igual ao tempo total, já que o tempo gasto buscando os dados no servidor é praticamente desprezível ($\sim1$ms) comparado ao tempo gasto transmitindo-os pela rede. Isso é devido ao fato que o banco está armazenado em um SSD, que permite um acesso aleatório extremamente rápido, e a conexão é sem fio, que é comparativamente bem mais lenta. No teste com Ethernet LAN o tempo de processamento é comparável ao de transmissão, já que o cliente e o servidor foram conectados diretamente com um cabo que suporta 1 Gbit/s e ambas as interfaces das máquinas suportam essa capacidade.

  \begin{table}[!ht]
      \begin{center}
          \caption{Estimativa do tempo médio do cliente, comunicação e processamento de cada servidor para cada tipo de enlace com um intervalo de confiança de 95\%.}\label{tab:metrics}
          \resizebox{\textwidth}{!}{
          \begin{tabular}{|c|c|c|c|c|c|c|}
          \hline
          \textbf{Enlace} & \textbf{Servidor} & \textbf{Perda (\%)} & \textbf{Tipo} & \boldmath $\mu_x$ \textbf{(ms)} & \boldmath $s_x$ \textbf{(ms)} & \textbf{IC de 95\% (ms)}\\
          \hline
          \multirow{6}{*}{Ethernet LAN}
          &     & \multirow{3}{*}{0}  & $T_p$ & 1,087 & 0,101 & [0,889; 1,285]  \\
          & TCP &                     & $T_c$ & 1,753 & 0,253 & [1,258; 2,249]  \\
          &     &                     & $T_t$ & 2,840 & 0,285 & [2,282; 3,399]  \\\cline{2-7}
          &     & \multirow{3}{*}{0}  & $T_p$ & 0,812 & 0,073 & [0,669; 0,956]    \\
          & UDP &                     & $T_c$ & 0,918 & 0,091 & [0,741; 1,096]  \\
          &     &                     & $T_t$ & 1,730 & 0,123 & [1,490; 1,971]  \\
          \hline
          \multirow{6}{*}{Wi-Fi LAN}
          &     & \multirow{3}{*}{0}  & $T_p$ & 1,153  & 0,084  & [0,987; 1,318]   \\
          & TCP &                     & $T_c$ & 42,167 & 5,560 & [31,269; 53,066]  \\
          &     &                     & $T_t$ & 43,320 & 5,569 & [32,405; 54,236]  \\\cline{2-7}
          &     & \multirow{3}{*}{0}  & $T_p$ & 1,016  & 0,760  & [0,867; 1,165]    \\
          & UDP &                     & $T_c$ & 14,984 & 3,996 & [7,151; 22,816]  \\
          &     &                     & $T_t$ & 16,000 & 4,011 & [8,139; 23,862]  \\
          \hline
          \multirow{6}{*}{Wi-Fi WAN}
          &     & \multirow{3}{*}{0}  & $T_p$ & 1,244  & 0,166  & [0,919; 1,570]    \\
          & TCP &                     & $T_c$ & 318,639 & 25,230 & [269,188; 368,090]  \\
          &     &                     & $T_t$ & 319,884 & 25,246 & [270,401; 369,366]  \\\cline{2-7}
          &     & \multirow{3}{*}{4}  & $T_p$ & 1,163  & 0,111  & [0,945; 1,381]    \\
          & UDP &                     & $T_c$ & 291,693 & 6,610 & [278,738; 304,648]  \\
          &     &                     & $T_t$ & 292,856 & 6,632 & [279,858; 305,855]  \\
          \hline
          \end{tabular}
          }
      \end{center}
  \end{table}

    As Figuras~\ref{fig:eth-stderr} a~\ref{fig:wan-stderr}, mostram alguns dos resultados da Tabela~\ref{tab:metrics} graficamente, onde é possível comparar diretamente o tempo médio e o intervalo de confiança para o servidor TCP e UDP nos diferentes testes. Note como o tempo médio total e de comunicação é sempre menor para o UDP que o TCP, como era de se esperar devido ao overhead menor do protocolo, que não precisa enviar ACKs ou realizar retransmissões para garantir a entrega total e ordenada dos pacotes. Além disso, o protocolo adotado na implementação era mais simples para o UDP, que não envia em um datagrama separado com o número de bytes de cada mensagem e não precisa enviar o tipo de operação que deseja realizar antes de mandar o email do perfil solicitado. Nessas figuras, também é possível notar que o intervalo de confiança de 95\% sempre é menor para as requisições para o servidor UDP. Isso ocorre porque não há muitos \emph{outliers} no caso dessas requisições, visto que o número de transmissões realizadas é sempre o mesmo. No TCP, caso a houvesse perdas, a requisição não iria falhar, mas iria demorar muito mais que as demais devido à necessidade de retransmitir vários pacotes. Já no UDP, quando há perdas, o cliente simplesmente dá um \emph{timeout} e esse tempo não é considerado no cálculo da média e do desvio padrão.

    Pela Tabela~\ref{tab:metrics}, temos que o UDP só tem perdas nos testes WAN, onde em 4 dos 100 testes a imagem não foi transmitida por completo. Nas redes LAN, o UDP não apresenta perdas em nenhum dos testes, chegando a ser quase três vezes mais rápido que o TCP para redes sem fio. Ainda assim, pode-se ver que o TCP é mais confiável que o UDP por nunca perder uma mensagem.

    Além de computar os intervalos para a nossa aproximação da média de cada variável, podemos também visualizar graficamente como as amostras se distribuem em torno da nossa estimativa. Como estamos assumindo uma distribuição normal para os dados, usamos a função de densidade de probabilidade dessa distribuição, dada pela Equação \ref{eq:normal-pdf}, para obter uma probabilidade relativa de cada amostra. Como trata-se de uma variável aleatório contínua, não faz sentido falarmos da probabilidade para amostras discretas, apenas para intervalos de valores. Dessa forma, a soma das probabilidades relativas de cada ponto não somam $1$, mas nos ajudam a ter uma intuição do quão provável é que uma nova amostra assuma esse valor.

    \begin{equation}\label{eq:normal-pdf}
        f(x) = \frac{1}{s_x \sqrt{2\pi}} e^{-\frac{(x-\mu)^2}{2s_x^2}}
    \end{equation}

    As Figuras~\ref{fig:eth} até~\ref{fig:wan} do Apêndice~\ref{sec:appendix} mostram o valor de $f(x)$ na Equação~\ref{eq:normal-pdf} para todas as amostras coletadas de $T_t$ e $T_c$, destacando em vermelho a área correspondente ao intervalo de confiança de 95\%. Por fim, os gráficos nas Figuras~\ref{fig:eth-stderr},~\ref{fig:wifi-stderr} e~\ref{fig:wan-stderr} permitem-nos comparar o tempo médio e o intervalo de confiança entre TCP e UDP. Note como, em geral, as distribuições para as requisições UDP aproximam-se mais de uma distribuição normal e apresentam poucos \emph{outliers} comparadas às requisições TCP. Em particular, nas Figuras~\ref{fig:wan-tcp-cli} e~\ref{fig:wan-tcp-com}, o intervalo de confiança de 95\% cobre apenas uma parte estreita à esquerda do gráfico, sinal que algumas requisições demoraram muito mais que as demais devido às perdas e a necessidade de realizar retransmissões.

  \begin{figure}[!ht]
    \centering
    \subfloat[]{
      \label{fig:eth-cli-std}
      \quad\includegraphics[width=0.45\textwidth]{./images/eth-lan-client-stderr.pdf}
    }
    \subfloat[]{
      \label{fig:eth-com-std}
      \quad\includegraphics[width=0.45\textwidth]{./images/eth-lan-communication-stderr.pdf}
    }
    \caption{Tempo de total do ponto de vista do cliente ($T_t$) médio~\protect\subref{fig:eth-cli-std} e tempo de comunicação ($T_c$) médio~\protect\subref{fig:eth-com-std} com um intervalo de confiança de 95\% para os servidores TCP e UDP no teste Ethernet LAN.}\label{fig:eth-stderr}
  \end{figure}

  \begin{figure}[!ht]
    \centering
    \subfloat[]{
      \label{fig:wifi-cli-std}
      \quad\includegraphics[width=0.45\textwidth]{./images/wifi-lan-client-stderr.pdf}
    }
    \subfloat[]{
      \label{fig:wifi-com-std}
      \quad\includegraphics[width=0.45\textwidth]{./images/wifi-lan-communication-stderr.pdf}
    }
    \caption{Tempo de total do ponto de vista do cliente ($T_t$) médio~\protect\subref{fig:wifi-cli-std} e tempo de comunicação ($T_c$) médio~\protect\subref{fig:wifi-com-std} com um intervalo de confiança de 95\% para os servidores TCP e UDP no teste Wi-Fi LAN.}\label{fig:wifi-stderr}
  \end{figure}

  \begin{figure}[!ht]
    \centering
    \subfloat[]{
      \label{fig:wan-cli-std}
      \quad\includegraphics[width=0.45\textwidth]{./images/wan-client-stderr.pdf}
    }
    \subfloat[]{
      \label{fig:wan-com-std}
      \quad\includegraphics[width=0.45\textwidth]{./images/wan-communication-stderr.pdf}
    }
    \caption{Tempo de total do ponto de vista do cliente ($T_t$) médio~\protect\subref{fig:wan-cli-std} e tempo de comunicação ($T_c$) médio~\protect\subref{fig:wan-com-std} com um intervalo de confiança de 95\% para os servidores TCP e UDP no teste WAN.}\label{fig:wan-stderr}
  \end{figure}
