#include "util.h"

#define PORT "1337"

int sockfd;                                            // Socket descriptor
int main(void)
{
    struct addrinfo hints, *servinfo, *curr;               // Server address info
    int status;
    MYSQL* connection;

    // Database
    connection = init_db();

    // Setting up hints for addrinfo
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;

    // Get own info
    if ((status = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0){
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        return 1;
    }

    // Bind to the first possible
    for (curr = servinfo; curr != NULL; curr = curr->ai_next){

        // Create socket
        if ((sockfd = socket(curr->ai_family, curr->ai_socktype, curr->ai_protocol)) < 0){
            perror("Error creating socket");
            continue;
        }

        int iSetOption = 1;
        setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char*)&iSetOption, sizeof(iSetOption));

        // Bind socket
        if (bind(sockfd, curr->ai_addr, curr->ai_addrlen) < 0){
            perror("Bind call error");
            continue;
        }

        break;
    }

    freeaddrinfo(servinfo);

    // Verifying if bound
    if (curr == NULL){
        fprintf(stderr, "Failed to bind server\n");
        exit(1);
    }

    printf("Receiving datagrams.....\n");

    // End execution
    signal(SIGINT, end);

    // Receiving UDP datagrams and returning profiles
    while(!manage_request(sockfd, connection)) {
      continue;
    };

    fprintf(stderr, "Error! Aborting server\n");
    close(sockfd);

    return 0;
}

MYSQL* init_db(void)
{
  printf("MySQL client version: %s\n", mysql_get_client_info());

  MYSQL *connection = mysql_init(NULL);

  if (connection == NULL)
  {
    fprintf(stderr, "%s\n", mysql_error(connection));
    exit(1);
  }

  connection = mysql_real_connect(connection,
    "localhost",  /* host */
    "root",       /* user */
    NULL,         /* password */
    "profiles",   /* db name */
    0,            /* port */
    NULL,         /* unix socket */
    0             /* client flags */
  );

  if (connection == NULL)
  {
    fprintf(stderr, "%s\n", mysql_error(connection));
    mysql_close(connection);
    exit(1);
  }

  return connection;
}

int manage_request(int sockfd, MYSQL* connection){
    struct sockaddr client_addr;
    char email[200];
    double proc_time;
    int num_bytes;
    FILE *log;

    /* Receives e-mail address corresponding to the profile. */
    num_bytes = recv_str(sockfd, email, &client_addr);

    if (num_bytes < 0){
        perror("Error in recv_str");
        return 1;
    }

    if(!send_profile(connection, sockfd, &client_addr, email, &proc_time)) {
      printf("Profile %s sent.\n", email);
    }

    log = fopen("logs/udp_proc_time.txt", "a");
    fprintf(log, "%g\n", proc_time);
    fflush(log);
    fclose(log);

    return 0;
}

void end(int signo){
    printf("\nEnding execution (%d)...", signo);
    close(sockfd);
    exit(0);
}
