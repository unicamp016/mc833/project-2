#ifndef LIB
#define LIB 1

#define _POSIX_C_SOURCE 200809L                 // Needed for compilation.
#define BUFFER_LENGTH 0x1 << 22                 // 4 MiB buffer size to send image in one go.
#define MAX_DGRAM_SIZE 0x1 << 14                // Send datagrams in chunks of 16 KiB

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <libgen.h>

// System libraries
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/poll.h>

#define SERVER_UNIT 0
#define CLIENT_UNIT 0
#define CLOCK_ID CLOCK_REALTIME

int send_str(int sockfd, char* buffer, int size, struct sockaddr* to);
int recv_str(int sockfd, char* buffer, struct sockaddr* from);
int recv_profile(int sockfd, char* buffer, struct sockaddr* from);
double elapsed_time(struct timespec t1, struct timespec t2, int micro);

struct experience {
  char description[200];
};

struct skill {
  char description[90];
};

typedef struct {
  int user_id;
  int nr_skills;
  int nr_experiences;
  char email[60];
  char name[30];
  char surname[30];
  char residence[30];
  char academic_degree[30];
  char photo[60];
  struct experience experiences[10];
  struct skill skills[10];
} profile;

#endif
