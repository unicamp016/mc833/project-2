#include "util.h"

#define PORT "1337"
#define BACKLOG 5

int sockfd;                                                // Socket descriptor
int main(void)
{
    int new_fd;                                            // New socket descriptor
    struct addrinfo hints, *servinfo, *curr, *cli_addr;    // Server address info
    socklen_t addrlen;
    int status;
    MYSQL* connection;

    // Database
    connection = init_db();

    // Setting up hints for addrinfo
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    // Get own info
    if ((status = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0){
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        return 1;
    }

    // Bind to the first possible
    for (curr = servinfo; curr != NULL; curr = curr->ai_next){

        // Create socket
        if ((sockfd = socket(curr->ai_family, curr->ai_socktype, curr->ai_protocol)) < 0){
            perror("server: socket");
            continue;
        }

        int iSetOption = 1;
        setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char*)&iSetOption, sizeof(iSetOption));

        // Bind socket
        if (bind(sockfd, curr->ai_addr, curr->ai_addrlen) < 0){
            perror("bind call error");
            continue;
        }

        break;
    }

    freeaddrinfo(servinfo);

    // Verifying if bound
    if (curr == NULL){
        fprintf(stderr, "server: failed to bind\n");
        exit(1);
    }

    // Listen for connection attempts
    if (listen(sockfd, BACKLOG) < 0){
        perror("listen call error");
        exit(1);
    }

    // Maybe reap zombie processes here?

    printf("Server ready and waiting for connections.....\n");

    // End execution
    signal(SIGINT, end);

    // Accepting connections concurrently with processes, FOREVER
    while(1){
        addrlen = sizeof cli_addr;
        new_fd = accept(sockfd, (struct sockaddr *)&cli_addr, &addrlen);

        // Verification of acceptance
        if (new_fd < 0){
            perror("Accept error");
            continue;
        }

        printf("Connection established...\n");

        // Create new process and do what the client wants in the child.
        if (fork() == 0){
            close(sockfd);
            satisfy_client(new_fd, connection);
            close(new_fd);
            printf("Connection finished...\n");
            exit(0);
        }

        close(new_fd);
    }

    return 0;
}

MYSQL* init_db(void)
{
  printf("MySQL client version: %s\n", mysql_get_client_info());

  MYSQL *connection = mysql_init(NULL);

  if (connection == NULL)
  {
    fprintf(stderr, "%s\n", mysql_error(connection));
    exit(1);
  }

  connection = mysql_real_connect(connection,
    "localhost",  /* host */
    "root",       /* user */
    NULL,         /* password */
    "profiles",   /* db name */
    0,            /* port */
    NULL,         /* unix socket */
    0             /* client flags */
  );

  if (connection == NULL)
  {
    fprintf(stderr, "%s\n", mysql_error(connection));
    mysql_close(connection);
    exit(1);
  }

  return connection;
}

int satisfy_client(int new_fd, MYSQL* connection)
{
    int choice, error;
    char buf[200], buf2[200];
    struct timespec t1, t2;
    double proc_time; // Time to retrieve data from database in microseconds
    FILE *log;

    // Receives a code for what the client wants.
    if (recv(new_fd, &choice, sizeof(choice), 0) <= 0) {
      return 1;
    }

    // Opens time log.
    log = fopen("logs/tcp_proc_time.txt", "a");

    // Keeps getting requests while the client wants.
    while(choice){

        switch(choice){

            // Lists all people from a certain course.
            case 1:
                recv_str(new_fd, buf);
                send_course_graduates(connection, new_fd, buf, &proc_time);
                break;

            // Lists the abilities of the people from a certain city.
            case 2:
                recv_str(new_fd, buf);
                send_residence_skills(connection, new_fd, buf, &proc_time);
                break;

            // Adds a new experience source on a profile.
            case 3:
                recv_str(new_fd, buf);                        // E-Mail
                recv_str(new_fd, buf2);                       // Experience source
                clock_gettime(CLOCK_ID, &t1);
                error = 0;
                if (insert_experience(connection, buf, buf2) != NULL) {
                  error = 1;
                }
                clock_gettime(CLOCK_ID, &t2);
                proc_time = elapsed_time(t1, t2, SERVER_UNIT);
                send(new_fd, &error, sizeof(int), 0);
                break;

            // Lists the experience sources from a profile.
            case 4:
                recv_str(new_fd, buf);
                send_user_experience(connection, new_fd, buf, &proc_time);
                break;

            // Lists all information from all profiles.
            case 5:
                send_all_profiles(connection, new_fd, &proc_time);
                break;

            //Gets all information from one profile.
            case 6:
                recv_str(new_fd, buf);
                send_profile(connection, new_fd, buf, &proc_time);
                break;

            // Easter egg
            case 42:
                send_str(new_fd, "You have found the Easter Egg!\n", 32);
                printf("A client has found the Easter Egg!\n");
                break;

            default:
                send_str(new_fd, "Invalid option. Try again.\n", 28);
                break;
        }

        if (choice >= 1 && choice <= 6) {
                fprintf(log, "%g\n", proc_time);
                fflush(log);
            }

        if (recv(new_fd, &choice, sizeof(choice), 0) <= 0) {
          break;
        }
    }

    // Closes time log.
    fclose(log);

    return 0;
}

void end(int signo){
    printf("\nEnding execution (%d)...", signo);
    close(sockfd);
    exit(0);
}
