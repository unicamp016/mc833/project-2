Tempo de Comunicação
Server type: tcp
Mean: 42.167423480000004
Standard Deviation: 5.5602636749944825
Interval: (31.269306677010817, 53.06554028298919)
Tempo do Cliente
Server type: tcp
Mean: 43.320248000000014
Standard Deviation: 5.569256786596077
Interval: (32.4045046982717, 54.235991301728326)
Tempo de Processamento
Server type: tcp
Mean: 1.15282452
Standard Deviation: 0.08448073867252914
Interval: (0.9872422722018429, 1.318406767798157)
Loss: 0/100

Tempo de Comunicação
Server type: udp
Mean: 14.98393395
Standard Deviation: 3.996161515818538
Interval: (7.151457378995667, 22.816410521004336)
Tempo do Cliente
Server type: udp
Mean: 16.000297999999997
Standard Deviation: 4.011042296188628
Interval: (8.138655099470286, 23.86194090052971)
Tempo de Processamento
Server type: udp
Mean: 1.0163640500000002
Standard Deviation: 0.07600104520963559
Interval: (0.8674020013891144, 1.165326098610886)
Loss: 0/100
