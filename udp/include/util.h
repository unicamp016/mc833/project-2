#ifndef UTIL
#define UTIL 1

#include "lib.h"

// MySQL
#include <mysql.h>

/* Prototypes for each of the six possible client requests */
MYSQL_RES* list_course_graduates(MYSQL* connection, const char* course);
MYSQL_RES* list_residence_skills(MYSQL* connection, const char* residence);
MYSQL_RES* insert_experience(MYSQL* connection, const char* email, const char* description);
MYSQL_RES* list_user_experience(MYSQL* connection, const char* email);
MYSQL_RES* list_all(MYSQL* connection);
MYSQL_RES* list_profile(MYSQL* connection, const char* email);

/* Wrappers to send strings with proper info to clients */
int send_profile(MYSQL* connection, int sock_fd, struct sockaddr* to, char* email, double* proc_time);
int send_course_graduates(MYSQL* connection, int sock_fd, char* course, double* proc_time);
int send_residence_skills(MYSQL* connection, int sock_fd, char* city, double* proc_time);
int send_user_experience(MYSQL* connection, int sock_fd, char* email, double* proc_time);
int send_all_profiles(MYSQL* connection, int sock_fd, double* proc_time);
void serialize_profile(profile profile_info, char *const profile_str);

/* Wrappers for mysql API */
MYSQL_RES* sql_query(MYSQL* connection, const char *query, int verbose);

/* Initializing database on server*/
MYSQL* init_db(void);

/* Manages requests. */
int manage_request(int sockfd, MYSQL* connection);

/* Misc */
int max(int a, int b);
void end(int signo);

#endif
